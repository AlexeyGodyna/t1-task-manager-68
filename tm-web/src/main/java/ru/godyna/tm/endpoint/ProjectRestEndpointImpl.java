package ru.godyna.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.godyna.tm.api.endpoint.IProjectRestEndpoint;
import ru.godyna.tm.model.Project;
import ru.godyna.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@RestController
@RequestMapping("/api/projects")
public class ProjectRestEndpointImpl implements IProjectRestEndpoint {

    @Autowired
    private ProjectService projectService;

    @Override
    @Nullable
    @GetMapping("/findAll")
    public Collection<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @Nullable
    @GetMapping("/findById/{id}")
    public Project findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        return projectService.findOneById(id);
    }

    @NotNull
    @Override
    @PostMapping("/save")
    public Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.add(project);
        return project;
    }

    @Override
    @PostMapping("/delete")
    public void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody final Project project
    ) {
        projectService.remove(project);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") final String id
    ) {
        projectService.removeById(id);
    }

}
