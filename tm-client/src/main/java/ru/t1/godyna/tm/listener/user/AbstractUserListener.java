package ru.t1.godyna.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.godyna.tm.api.endpoint.IAuthEndpoint;
import ru.t1.godyna.tm.api.endpoint.IUserEndpoint;
import ru.t1.godyna.tm.listener.AbstractListener;

@Component
public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected IAuthEndpoint authEndpoint;

}
