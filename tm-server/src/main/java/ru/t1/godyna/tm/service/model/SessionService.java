package ru.t1.godyna.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.godyna.tm.api.repository.model.ISessionRepository;
import ru.t1.godyna.tm.api.repository.model.IUserRepository;
import ru.t1.godyna.tm.api.service.ISessionService;
import ru.t1.godyna.tm.exception.entity.EntityNotFoundException;
import ru.t1.godyna.tm.exception.entity.UserNotFoundException;
import ru.t1.godyna.tm.exception.field.IdEmptyException;
import ru.t1.godyna.tm.exception.field.UserIdEmptyException;
import ru.t1.godyna.tm.exception.user.AccessDeniedException;
import ru.t1.godyna.tm.model.Session;
import ru.t1.godyna.tm.model.User;

import java.util.List;

@Service
@NoArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository sessionRepository;

    @NotNull
    @Autowired
    private IUserRepository userRepository;

    @NotNull
    @Override
    @Transactional
    public Session add(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @Override
    @Transactional
    public @NotNull Session add(@Nullable String userId, @Nullable Session model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new AccessDeniedException();
        @Nullable User user = userRepository.findById(userId).orElse(null);
        if(user == null) throw new UserNotFoundException();
        model.setUser(user);
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public List<Session> add(@NotNull List<Session> models) {
        if (models == null) throw new EntityNotFoundException();
        sessionRepository.saveAll(models);
        return models;
    }

    @Override
    @Transactional
    public void clear(@NotNull String userId) {
        if (userId == null || userId.isEmpty()) throw new UserNotFoundException();
        sessionRepository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull String id) {
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        return sessionRepository.findAll();
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return sessionRepository.findByUserIdAndId(userId, id);
    }

    @NotNull
    @Override
    @Transactional
    public Session update(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.saveAndFlush(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Session remove(@Nullable Session model) {
        if (model == null) throw new AccessDeniedException();
        sessionRepository.delete(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public Session removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.delete(session);
        return session;
    }

    @Override
    @Transactional
    public @NotNull Session removeById(@Nullable String userId, @Nullable String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session session = findOneById(userId, id);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.delete(session);
        return session;
    }

}
