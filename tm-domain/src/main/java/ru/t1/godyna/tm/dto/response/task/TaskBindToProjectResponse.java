package ru.t1.godyna.tm.dto.response.task;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class TaskBindToProjectResponse extends AbstractTaskResponse {
}
